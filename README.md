## To Run:

```sh
python3 -mvenv venv
. venv/bin/activate
pip install wheel libvirt flask
vi config.py #put the following line in the file (ohne hostname heist localhost):
vm_hosts = [ "qemu:///system", "qemu+ssh://<your hypervisor>/system"]
FLASK_ENV=development FLASK_APP=fmv.py flask run --host=0.0.0.0
```

And that's about it, I think. For the system that will run this, one needs libvirt & libvirt-dev, at least. (debian & ubuntu systems have one available via apt)

You should see something simillar to the following:
![screenshot](/documentation/screenshot-0.png "Screenshot of the Kitties")