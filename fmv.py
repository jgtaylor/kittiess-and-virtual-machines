import flask
import libvirt
#import config
import xml.etree.ElementTree as ET
'''
TODO:
    - Add a WebSocket for the browser to get realtime info
    - restructure this whole thing, i hate d_struct()
    - add event listners (or, signals, i don't know python lingo)
        for the domain & hypervisor lifecycle events
        - publish the events on the websocket
    - add login/session stuff (?)[i haven't the foggiest idea
        how to / where to start on this. Research is in order]
    - add certificates (https, as well as certs for libvirt's use)
    - add network & storage pool stuff (not high prio)
'''
config = { "vm_hosts": [ "qemu:///system", "qemu+ssh://lenny.lan/system", "qemu+ssh://pacer.lan/system"] }

app = flask.Flask(__name__)
conn_list = []
for hypervisor in config["vm_hosts"]:
    try:
        c = libvirt.openReadOnly(hypervisor)
        conn_list.append(c)
    except libvirt.libvirtError:
        print(libvirt.libvirtError)


def d_struct(hypervisor_list: list) -> dict:
    '''Returns a dict of hypervisor connections & details\n
    *hypervisor_list is a list of libvirt connection strings:
    "qemu+ssh://hyper.example.com/system"
    '''
    struct = dict()
    state_map = {0: "no state", 1: "running", 2: "blocked on resource",
                 3: "paused", 4: "in process shutting down", 5: "shutdown",
                 6: "crashed", 7: "suspended", 8: "nb"}
    for h in hypervisor_list:
        domains = h.listAllDomains()
        h_name = h.getHostname()
        struct[h_name] = dict()
        struct[h_name]['domains'] = dict()
        struct[h_name]['conn'] = h
        for d in domains:
            state, maxmem, mem, cpus, cput = d.info()
            state = state_map[state]
            name = d.name()
            ver = d._conn.getVersion()
            hypervisor = h_name
            if ver > 2010000 and state == "running":
                ipaddrs = d.interfaceAddresses(0)
            else:
                ipaddrs = "No network info avail."
            struct[h_name]['domains'][name] = {
                                        "state": state,
                                        "ver": ver,
                                        "hypervisor": hypervisor,
                                        "ipaddrs": ipaddrs,
                                        "maxmem": maxmem,
                                        "mem": mem,
                                        "cpus": cpus,
                                        "cput": cput
                                    }
    return struct


def getCPUInfo(dom: libvirt.virDomain) -> dict:
    '''get a dict of cpu info including total time use, cpu count and
    individual cpu details\n
    @dom is a virDomain object returned from a hypervisor connection
    '''
    if (dom.state()[0] == 1):
        cpu_time_total = dom.getCPUStats(True)[0]['cpu_time'] / 1000000000
        cpus = []
        for el in dom.vcpus()[0]:  # eg [(0, 1, 269300000000, 2)]
            (cpuid, state, time, realcpu_id) = el
            cpus.append({'cpu_id': cpuid, 'cpu_state': state,
                        'cpu_time': time / 1000000000})
        cpu_count = len(cpus)
        return {'cpu_time_total': cpu_time_total,
                'cpu_count': cpu_count, 'cpus': cpus}
    else:
        cpu_count = int(ET.fromstring(dom.XMLDesc()).find('vcpu').text)
        return {'cpu_time_total': None, 'cpu_count': cpu_count, 'cpus': None}


hypervisors = d_struct(conn_list)


@app.route('/')
def index():
    '''Renders the default page'''
    kw = dict()
    kw['hypervisors'] = d_struct(conn_list)
    # clean out the conn from the list
    for hyper in kw['hypervisors']:
        kw['hypervisors'][hyper]['conn'] = None

    return flask.render_template('index.jinja2', **kw)


@app.route('/hypervisor/<hypervisor>')
def hypervisor_detail(hypervisor: str) -> dict:
    '''return JSON info for a specific hypervisor'''
    conn = hypervisors[hypervisor]['conn']
    (arch, totalMem, activeCPUs, freq, NUMAnodes,
        CPUSockPerNode, coresPerSock, threadsPerCore) = conn.getInfo()
    numActiveDomains = hypervisors[hypervisor]['conn'].numOfDomains()
    numDefinedDomains = hypervisors[hypervisor]['conn'].numOfDefinedDomains()
    return {
        "hyperHostname": hypervisor,
        "hyperArch": arch,
        "hyperTotalMem": totalMem,
        "hyperActiveCPUs": activeCPUs,
        "hyperFreq": freq,
        "hyperNUMAnodes": NUMAnodes,
        "hyperSockPerNode": CPUSockPerNode,
        "hyperCoresPerSock": coresPerSock,
        "hyperThreadPerCore": threadsPerCore,
        "hyperNumDomains": numActiveDomains + numDefinedDomains,
        "hyperActiveDomains": numActiveDomains,
        "hyperDefinedDomains": numDefinedDomains
    }


@app.route('/hypervisors/<hypervisor>/<domain>/')
def domain_info(hypervisor: str, domain: str) -> dict:
    '''Return JSON for a specific domain'''
    conn = hypervisors[hypervisor]['conn']
    dom = conn.lookupByName(domain)
    domXML = dom.XMLDesc()
    disks = getDiskFromXML(domXML)
    for disk in disks:
        if (type(disk['file']) is str):
            try:
                vol = conn.storageVolLookupByPath(disk['file'])
                (disk['backing-type'], disk['capacity'],
                    disk['allocation']) = vol.info()
                disk['capacity'] = (disk['capacity']/1024)/1024
                disk['allocation'] = (disk['allocation']/1024)/1024
            except (AttributeError, libvirt.libvirtError):
                pass

    spice_info = getSpiceFromXML(domXML)
    nics = getNicsFromXML(domXML)
    cpu = getCPUInfo(dom)
    mem = None
    accounting = None
    # the return below should ideally provide only strings and lists
    # , but it doesn't... cpu info doesn't really make sense to go into a list.
    # neither does accounting ...
    return {"hypervisor": hypervisor, "domain": domain,
            'spice_info': spice_info, 'disks': disks,
            'interfaces': nics, 'cpu': cpu, 'memory': mem,
            'accounting': accounting}


def build_json_attrib(dev, key, attr):
    '''Generate the dict from xml, set null if no data'''
    try:
        a = dev.find(key).attrib
    except AttributeError:
        a = {attr: None}
    return a


def getNicsFromXML(xml: str) -> list:
    '''returns an array of dicts for Network info\n
    *xml is a string of XML data, usually retrieved from <domain>.XMLDesc()
    '''
    dom = ET.fromstring(xml)
    nic_list = []
    try:
        nics = dom.find('devices').findall('interface')
        if (len(nics) < 1):
            raise UserWarning('No interfaces found.')
    except UserWarning:
        nics = None
        return nics
    else:
        for nic in nics:
            mac = build_json_attrib(nic, 'mac', 'address')
            target = build_json_attrib(nic, 'target', 'dev')
            source = build_json_attrib(nic, 'source', 'network')
            model = build_json_attrib(nic, 'model', 'type')
            nic_list.append({'mac': mac['address'], 'source': source,
                            'device': target['dev'], 'model': model['type']})
        return nic_list


def getSpiceFromXML(xml: str) -> dict:
    '''Returns a dict from the Domain's xml'''
    dom = ET.fromstring(xml)
    try:
        return dom.find('devices').find('graphics').attrib
    except AttributeError:
        return None


def getDiskFromXML(xml: str) -> list:
    '''Returns an array of dicts for disks\n
    volInfo() returns [ type, capacity, allocation]
    unless flags= passes VIR_STORAGE_VOL_GET_PHYSICAL\n
    Storage types are:\n
    0 (0x0) Regular file based volumes\n
    1 (0x1) Block based volumes\n
    2 (0x2) Directory-passthrough based volume\n
    3 (0x3) Network volumes like RBD (RADOS Block Device)\n
    4 (0x4) Network accessible directory that can contain other network volumes\n
    5 (0x5) Ploop based volumes\n
    VIR_STORAGE_VOL_LAST 	= 6 (0x6)
    '''
    dom = ET.fromstring(xml)
    disk_list = []
    try:
        disks = dom.find('devices').findall('disk')
        if (len(disks) < 1):
            raise UserWarning('No disks found')
    except UserWarning:
        return None
    else:
        for disk in disks:
            disk_type = disk.attrib
            source = build_json_attrib(disk, 'source', 'file')
            target = build_json_attrib(disk, 'target', 'dev')
            disk_list.append({'type': disk_type['device'],
                              'file': source['file'], 'dev': target['dev']})
        return disk_list
