let hyper = document.getElementsByClassName('hypervisor');
let detail = document.getElementById('hypervisorDetail');
function getHypervisorJson(hypervisor) {
    let url = "/hypervisor/" + hypervisor;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                let hd = JSON.parse(this.responseText);
                let ids = ["hyperHostname","hyperArch","hyperTotalMem","hyperActiveCPUs","hyperFreq",
                "hyperNUMAnodes","hyperSockPerNode","hyperCoresPerSock","hyperThreadPerCore","hyperNumDomains","hyperSupportedArch" ]
                ids.forEach(e => {
                    let el = document.getElementById(e);
                    if (Array.isArray(hd[e])) { 
                        el.innerText =   hd[e].join(" ");
                    } else {
                        el.innerText = hd[e];
                    }
                });
            }
            if (this.status == 404) {
                console.log(hypervisor + ": Page not found.");
            }
        }
    }
    xhttp.open("GET", url, true);
    xhttp.send();
    /* Exit the function: */
    return;
}

function makeHover(element) {
    console.log("should hover a floated div");
    element.addEventListener('click', event => {
        getHypervisorJson(event.target.id);
        console.log(`left: ${event.clientX}, top: ${event.clientY}`);
        detail.style.left = event.clientX + "px";
        detail.style.top = event.clientY + "px";
        detail.classList.remove("w3-hide");
        detail.classList.add("w3-show");
        console.log(event);
    }, {"passive": true}, true);
    return;
}

for (let i = 0; i <= hyper.length - 1; i++) {
    makeHover(hyper[i])
}

detail.addEventListener('click', () => {
    detail.classList.remove("w3-show");
    detail.classList.add("w3-hide");
    console.log(event);
}, {"passive": true}, false);